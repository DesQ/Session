/**
 * This file is a part of DesQ Session.
 * DesQSession is the Session Manager for DesQ DE.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDir>

#include <signal.h>
#include <unistd.h>

#include <DFUtils.hpp>
#include <DFXdg.hpp>

#include <desq/desq-config.h>

#include <DFCoreApplication.hpp>
#include <DFLogin1.hpp>

#include "SessionManager.hpp"

void houseKeeping() {
    /**
     * We first test if /tmp is writable. If not, exit.
     *
     * Then, this function will check if all the necessary folders exist.
     * Typically, we need ~/.config/DesQ, ~/.cache/DesQ.
     * We will use QDir::mkpath(...), which does not complain if the
     * directories exist.
     *
     * fontconfig is the best way to ensure uniform fonts across all
     * apps.
     *
     * Check if a runtime location has been set. Otherwise, set it up.
     *
     * Based on other needs, eventually, we can expand this function
     * to perform more work.
     */

    if ( DFL::Utils::isWritable( "/tmp" ) == false ) {
        qCritical() << "Unable to write in /tmp. Exiting.";
        qApp->quit();
    }

    QDir cache( DFL::XDG::xdgCacheHome() );

    /** Place where DesQ Component logs live */
    cache.mkpath( "DesQ" );

    /** Place where Wayfire logs live */
    cache.mkpath( "DesQ/Wayfire" );

    QDir config( DFL::XDG::xdgConfigHome() );

    config.mkpath( "DesQ" );

    /** For fontconfig/fonts.conf */
    config.mkpath( "fontconfig" );

    /** We can blindly do this: QFile::copy will not over-write existing files. */
    QFile::copy( ConfigPath "fonts.conf", config.filePath( "fontconfig/fonts.conf" ) );

    /** Runtime location: Possibility is that (e)logind will have set it up, but still. */
    QString runtimeDir( QStandardPaths::writableLocation( QStandardPaths::RuntimeLocation ) );

    if ( runtimeDir.isEmpty() ) {
        QString runtimeDir = QString( "/run/user/%1" ).arg( getuid() );

        if ( QDir( "/" ).mkpath( runtimeDir ) ) {
            qputenv( "XDG_RUNTIME_DIR", runtimeDir.toUtf8() );
        }

        else {
            QString runtimeDir = QString( "/tmp/%1" ).arg( getuid() );

            if ( QDir( "/" ).mkpath( runtimeDir ) ) {
                qputenv( "XDG_RUNTIME_DIR", runtimeDir.toUtf8() );
            }

            else {
                qCritical() << "Unable to create a runtime directory.";
                exit( 1 );
            }
        }
    }
}


int main( int argc, char *argv[] ) {
    houseKeeping();

    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Session.log" ).toLocal8Bit().data(), "a" );

    qInstallMessageHandler( DFL::Logger );

    QByteArray date = QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8();

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Session started at" << date.constData();
    qDebug() << "------------------------------------------------------------------------\n";

    DFL::CoreApplication app( argc, argv );

    // Set application info
    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Session Manager" );
    app.setApplicationVersion( PROJECT_VERSION );

    app.interceptSignal( SIGTERM, nullptr );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    parser.addOption( { "logout", "Logout from the current session." } );
    parser.addOption( { "suspend", "Suspend the computer." } );
    parser.addOption( { "hibernate", "Hibernate the computer." } );
    parser.addOption( { "shutdown", "Shutdown the computer" } );
    parser.addOption( { "reboot", "Reboot the computer" } );
    parser.addOption( { "reset", "Reset the current session" } );
    parser.addOption( { "safemode", "Start the Session in Safe Mode" } );

    /** Option for testing purposes only */
    // QCommandLineOption testOpt( "kr", "Run DesQ Session in testing mode." );
    // testOpt.setFlags( QCommandLineOption::HiddenFromHelp );
    //
    // parser.addOption( testOpt );

    /** Select a session to be started */
    parser.addOption( { "session", "Start a specific session", "session" } );

    parser.process( app );

    if ( app.lockApplication() ) {
        DesQ::Session::Manager *session = new DesQ::Session::Manager();

        QObject::connect( &app, &DFL::CoreApplication::messageFromClient, session, &DesQ::Session::Manager::handleMessages );

        qDebug() << "Starting session";
        session->start();

        return app.exec();
    }

    else {
        if ( parser.isSet( "logout" ) ) {
            app.messageServer( "logout" );
        }

        else if ( parser.isSet( "suspend" ) ) {
            app.messageServer( "suspend" );
        }

        else if ( parser.isSet( "hibernate" ) ) {
            app.messageServer( "hibernate" );
        }

        else if ( parser.isSet( "shutdown" ) ) {
            app.messageServer( "shutdown" );
        }

        else if ( parser.isSet( "reboot" ) ) {
            app.messageServer( "reboot" );
        }

        else if ( parser.isSet( "reset" ) ) {
            app.messageServer( "reset" );
        }

        else if ( parser.isSet( "session" ) ) {
            app.messageServer( "session\n" + parser.value( "session" ) );
        }

        else {
            parser.showHelp();
            qDebug() << 0;
        }

        return 0;
    }

    return 0;
}
