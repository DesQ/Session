/**
 * This file is a part of DesQ Session.
 * DesQSession is the Session Manager for DesQ DE.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>

#include <QDir>
#include <QProcess>

#include "SessionManager.hpp"

#include <DFUtils.hpp>
#include <DFXdg.hpp>
#include <DFCoreApplication.hpp>

#include <desq/desq-config.h>
#include <desq/Utils.hpp>

#include <DFLogin1.hpp>

DesQ::Session::Manager::Manager() : QObject() {
    session = new QSettings( "DesQ", "Session" );
    defSett = new QSettings( ConfigPath "Session.conf", QSettings::IniFormat );
    // mSessionName = "DesQ";

    int locks = session->value( "AcquireLocks", defSett->value( "AcquireLocks" ) ).toInt();

    login1 = new DFL::Login1( this );
    login1->acquireInhibitLocks( DFL::Login1::InhibitorLocks( locks ) );
}


void DesQ::Session::Manager::start( QString name ) {
    /**
     * DesQ Session crashed last time
     * This is a cue for DesQ Shell to show a message to the user.
     */
    if ( session->value( "SafeQuit", true ) == true ) {
        session->setValue( "__DESQ_SESSION_CRASH", "1" );
    }

    /** We just started the session */
    session->setValue( "SafeQuit", false );

    /** Store the session name for later */
    mSessionName = name;

    /** Setup the environmental variables */
    setupEnvironmentVars();

    /** Start the window manager and hence the shell */
    startDesQShell();
}


void DesQ::Session::Manager::stop() {
    /** Kill all processes that belong to the current session */
    killSessionProcesses();

    /** Stop wayfire */
    stopWayfire();

    /** Release the inhibitor locks */
    login1->releaseInhibitLocks();

    /** Remove temporary folder */
    DFL::Utils::removeDir( qgetenv( "__DESQ_WORK_DIR" ) );

    /** Mark that we exited safely */
    session->setValue( "SafeQuit", true );
    session->sync();
}


bool DesQ::Session::Manager::handleMessages( const QString msg, int fd ) {
    if ( msg == "logout" ) {
        qInfo() << "Trying to logout from the active session.";
        stop();

        qApp->quit();

        return true;
    }

    else if ( msg.startsWith( "Can" ) ) {
        int ret = login1->request( msg );

        if ( ( ret == -2 ) or ( ret == 0 ) ) {
            qApp->messageClient( "false", fd );
            return false;
        }

        else if ( ( ret == -1 ) or ( ret == 1 ) ) {
            qApp->messageClient( "true", fd );
        }

        else {
            qWarning() << "Invalid query";
            qApp->messageClient( "false", fd );
            return true;
        }
    }

    else if ( msg == "suspend" ) {
        /** By default, we would like to use dbus and connect to org.freedesktop.login1 */
        if ( session->value( "Commands/Suspend", defSett->value( "Commands/Suspend" ) ).toString() == "dbus" ) {
            return login1->request( "Suspend" );
        }

        /** It's surely not 'dbus', so it's got to be a command: run the command. */
        else {
            return QProcess::startDetached( session->value( "Commands/Suspend" ).toString(), QStringList() );
        }
    }

    else if ( msg == "hibernate" ) {
        /** By default, we would like to use dbus and connect to org.freedesktop.login1 */
        if ( session->value( "Commands/Hibernate", defSett->value( "Commands/Hibernate" ) ).toString() == "dbus" ) {
            return login1->request( "Hibernate" );
        }

        /** It's surely not 'dbus', so it's got to be a command: run the command. */
        else {
            return QProcess::startDetached( session->value( "Commands/Hibernate" ).toString(), QStringList() );
        }
    }

    else if ( msg == "shutdown" ) {
        stop();

        /** By default, we would like to use dbus and connect to org.freedesktop.login1 */
        if ( session->value( "Commands/Shutdown", defSett->value( "Commands/Shutdown" ) ).toString() == "dbus" ) {
            int ret = login1->schedulePowerEventIn( "poweroff", 1 * 1000 * 1000 );
            qDebug() << "PowerOff request" << ( ret ? "successful" : "failed" );
            return ret;
        }

        /** It's surely not 'dbus', so it's got to be a command: run the command. */
        else {
            return QProcess::startDetached( session->value( "Commands/Shutdown" ).toString(), QStringList() );
        }
    }

    else if ( msg == "reboot" ) {
        stop();

        /** By default, we would like to use dbus and connect to org.freedesktop.login1 */
        if ( session->value( "Commands/Reboot", defSett->value( "Commands/Reboot" ) ).toString() == "dbus" ) {
            int ret = login1->schedulePowerEventIn( "reboot", 1 * 1000 * 1000 );
            qDebug() << "Reboot request" << ( ret ? "successful" : "failed" );
            return ret;
        }

        /** It's surely not 'dbus', so it's got to be a command: run the command. */
        else {
            return QProcess::startDetached( session->value( "Commands/Reboot" ).toString(), QStringList() );
        }
    }

    else if ( msg == "reset" ) {
        /** Kill the existing processes */
        killSessionProcesses();

        /** Stop wayfire */
        stopWayfire();

        /** Sleep for a second */
        QThread::sleep( 1 );

        /**
         * Set the environmental variables again.
         * Opportunity to reset/change global environment.
         */
        setupEnvironmentVars();

        /** Start Wayfire, and subsequently DesQ Shell again */
        startDesQShell();
    }

    else if ( msg == "kr" ) {
        /** Kill the existing processes */
        killSessionProcesses();
    }

    else if ( msg.startsWith( "session" ) ) {
        QStringList args = msg.split( "\n", Qt::SkipEmptyParts );

        if ( args.count() == 2 ) {
            start( args[ 1 ] );
        }

        else if ( args.count() == 1 ) {
            start();
        }

        return true;
    }

    return false;
}


void DesQ::Session::Manager::setupEnvironmentVars() {
    /** Wayland */
    qputenv( "XDG_SESSION_TYPE", "wayland" );

    /** Check and set the default XDG_*_HOME paths */
    session->beginGroup( "XDG_HOME" );
    defSett->beginGroup( "XDG_HOME" );
    for ( QString var: defSett->allKeys() ) {
        QString value = session->value( var, defSett->value( var ) ).toString();
        value.replace( "$HOME", QDir::homePath() );

        /** If this variable is not set, set it */
        if ( not qgetenv( var.toUtf8() ).length() ) {
            qputenv( var.toUtf8(), value.toUtf8() );
        }

        /** If this directory does not exist, create it */
        if ( not QFile::exists( qgetenv( var.toUtf8() ) ) ) {
            QDir::home().mkpath( QDir::home().filePath( qgetenv( var.toUtf8() ) ) );
        }
    }
    session->endGroup();
    defSett->endGroup();

    /** Check and set the default XDG_*_DIRS paths */
    session->beginGroup( "XDG_DIRS" );
    defSett->beginGroup( "XDG_DIRS" );
    for ( QString var: defSett->allKeys() ) {
        QString value = session->value( var, defSett->value( var ) ).toString();
        value.replace( "$HOME", QDir::homePath() );

        /** If this variable is not set, set it */
        if ( not qgetenv( var.toUtf8() ).length() ) {
            qputenv( var.toUtf8(), value.toUtf8() );
        }
    }
    session->endGroup();
    defSett->endGroup();

    /**
     * Check if the user folders like Documents, Downloads etc are set
     * Also, set the corresponding values in ~/.config/user-dirs.dirs
     */
    QString XDG_CONFIG_HOME( qgetenv( "XDG_CONFIG_HOME" ) );

    if ( not XDG_CONFIG_HOME.endsWith( "/" ) ) {
        XDG_CONFIG_HOME += "/";
    }

    QString userDirStr;

    session->beginGroup( "XDG_USER" );
    defSett->beginGroup( "XDG_USER" );
    for ( QString var: defSett->allKeys() ) {
        QString value = session->value( var, defSett->value( var ) ).toString();
        value.replace( "$HOME", QDir::homePath() );
        QString origValue( qgetenv( var.toUtf8() ) );

        /** Unset if this variable has already been set */
        if ( origValue.length() ) {
            unsetenv( var.toUtf8().constData() );
        }

        /**
         * Replace it with our values
         * Set it only if the folder exists
         */
        if ( QFile::exists( value ) ) {
            qputenv( var.toUtf8(), value.toUtf8() );
            userDirStr += var + "=" + value + "\n";
        }

        else {
            qputenv( var.toUtf8(), QDir::homePath().toUtf8() );
            userDirStr += var + "=" + QDir::homePath() + "\n";
        }


        /** If the folder does not exist, reset it to $HOME */
        if ( not QFile::exists( qgetenv( var.toUtf8() ) ) ) {
            qputenv( var.toUtf8(), QDir::homePath().toUtf8() );
        }
    }
    session->endGroup();
    defSett->endGroup();

    QFile userDirs( XDG_CONFIG_HOME + "user-dirs.dirs" );

    if ( not userDirs.open( QFile::WriteOnly ) ) {
        qWarning() << "Unable to write to" << userDirs.fileName();
    }

    userDirs.write( userDirStr.toUtf8() );
    userDirs.close();

    /** Platform for Qt5 */
    qputenv( "QT_QPA_PLATFORM",                     QByteArrayLiteral( "wayland" ) );

    /** Qt5 theme to be used */
    qputenv( "QT_QPA_PLATFORMTHEME",                QByteArrayLiteral( "desq" ) );

    /** Revise: Wayfire SSD with windecor does quite well */
    qputenv( "QT_WAYLAND_DISABLE_DECORATION",       "1" );
    qputenv( "QT_WAYLAND_DISABLE_WINDOWDECORATION", "1" );

    /** For Mozilla firefox and thunderbird */
    qputenv( "MOZ_ENABLE_WAYLAND",                  "1" );

    /** Desktop session */
    qputenv( "DESKTOP_SESSION",                     "/usr/share/wayland-sessions/desqsession" );

    /** Desktop session name */
    qputenv( "XDG_SESSION_DESKTOP",                 "DesQ" );
    qputenv( "XDG_CURRENT_DESKTOP",                 "DesQ:Wayfire:wlroots" );

    /** We need to start wayfire in safe-mode this time */
    if ( session->value( "SafeMode" ) == true ) {
        wfConfig   = "/tmp/wayfire.ini";
        wfConfigHj = "/tmp/wayfire.hjson";

        QFile::copy( "/usr/share/desq/configs/wayfire-safe.ini",   wfConfig );
        QFile::copy( "/usr/share/desq/configs/wayfire-safe.hjson", wfConfigHj );

        /**
         * This is a cue for DesQ Shell to inform the user that
         * they need to manually switch off the SafeMode flag.
         */
        qputenv( "__DESQ_SAFE_MODE", "1" );
    }

    else {
        wfConfig   = QDir::home().filePath( ".config/DesQ/wayfire.ini" );
        wfConfigHj = QDir::home().filePath( ".config/DesQ/wayfire.hjson" );
    }

    /**
     * Whether to use hjson backend or not.
     * If @hjsonBackend is not empty, we will use it.
     */
    QString hjsonBackend = session->value( "HjsonBackend" ).toString();

    /** Specify the config backend. Also make the config file hjson. */
    if ( hjsonBackend.size() ) {
        qputenv( "WAYFIRE_CONFIG_FILE", wfConfigHj.toUtf8().constData() );
    }

    else {
        qputenv( "WAYFIRE_CONFIG_FILE", wfConfig.toUtf8().constData() );
    }

    if ( QFile::exists( wfConfig ) == false ) {
        bool ok = true;
        ok &= QDir::home().mkpath( ".config/DesQ/" );
        ok &= QFile::copy( "/usr/share/desq/configs/wayfire.ini", wfConfig );

        /** If we're unable to copy the config file to user's config dir, use the default */
        if ( not ok ) {
            qWarning() << "Error copying wayfire.ini to ~/.config/DesQ/";
            wfConfig = "/usr/share/desq/wayfire.ini";
        }
    }

    if ( QFile::exists( wfConfigHj ) == false ) {
        bool ok = true;
        ok &= QDir::home().mkpath( ".config/DesQ/" );
        ok &= QFile::copy( "/usr/share/desq/configs/wayfire.hjson", wfConfigHj );

        /** If we're unable to copy the config file to user's config dir, use the default */
        if ( not ok ) {
            qWarning() << "Error copying wayfire.hjson to ~/.config/DesQ/";
            wfConfigHj = "/usr/share/desq/wayfire.hjson";
        }
    }

    /** Cursor theme and size */
    qputenv( "XCURSOR_THEME", session->value( "CursorTheme", "bloom" ).toByteArray() );
    qputenv( "XCURSOR_SIZE",  session->value( "CursorSize", "24" ).toByteArray() );

    /** Initialize the DesQ Work Dir */
    DesQ::Utils::prepareSessionVariables();

    /** Set the DesQ Session Name */
    qputenv( "__DESQ_SESSION_NAME", mSessionName.toUtf8() );

    /*
     * Custom Environment Variables: Special env variables for DesQ Session
     * For now we do everything from our settings file, section [Environment]. Format: Var = value
     */
    session->beginGroup( "Environment" );
    for ( QString var: session->allKeys() ) {
        qputenv( var.toUtf8(), session->value( var ).toString().toUtf8() );
    }

    session->endGroup();
}


void DesQ::Session::Manager::startDesQShell() {
    qInfo() << "Starting window manager: wayfire";

    /** Create a tracked process for wayfire */
    wfProc = new QProcess( this );
    wfProc->setWorkingDirectory( QDir::homePath() );
    wfRestartCount = 0;

    QFile       *wfLog = new QFile();   // Log file into which we will write the wayfire's logs.
    QStringList wfArgs;                 // Args provided to wayfire when starting the session.

    /** Start wayfire in debug mode? Default is always false */
    if ( session->value( "WayfireDebug", false ).toBool() ) {
        wfArgs << "--debug";
    }

    /**
     * Whether to use hjson backend or not.
     * If @hjsonBackend is not empty, we will use it.
     */
    QString hjsonBackend = session->value( "HjsonBackend" ).toString();

    /** Specify the config backend. Also make the config file hjson. */
    if ( hjsonBackend.size() ) {
        wfArgs << "--config" << wfConfigHj;
        wfArgs << "--config-backend" << hjsonBackend;
    }

    else {
        wfArgs << "--config" << wfConfig;
    }

    /** Store the wayfire logs in ~/.cache/DesQ/Wayfire/ */
    connect(
        wfProc, &QProcess::readyRead, [ = ] () {
            if ( wfLog->open( QFile::WriteOnly | QFile::Append ) == false ) {
                qWarning() << "Error opening log file:" << wfLog->error();
                qWarning() << wfProc->readAll().trimmed().constData();
                return;
            }

            wfLog->write( wfProc->readAll() );
            wfLog->close();
        }
    );

    /** Log file template */
    QString templ( DFL::XDG::xdgCacheHome() + "/DesQ/Wayfire/%1.log" );

    /**
     * Since desq-session is responsible for starting/stopping wayfire,
     * any termination of wayfire will be treated as a crash. We will
     * attempt to restart it upto 4 times: in all wayfire is started
     * five times. After 5 attempts at starting wayfire we will give up
     * and end the session.
     * A corresponding error message will show up in DesQ Shell. This
     * is based on __WAYFIRE_RESTART_COUNT env variable.
     */
    connect(
        wfProc, QOverload<int, QProcess::ExitStatus>::of( &QProcess::finished ), [ = ](int, QProcess::ExitStatus) mutable {
            if ( wfRestartCount < 5 ) {
                /**
                 * If the wayfire process was alive for more than a minute, then most likely it's
                 * alright.
                 * So, we reset the restart count, and the last start time.
                 */
                if ( mStartDateTime.secsTo( QDateTime::currentDateTime() ) > 60 ) {
                    wfRestartCount = 1;
                    mStartDateTime = QDateTime::currentDateTime();
                }

                /**
                 * This crash occurred in under a minute.
                 */
                else {
                    wfRestartCount++;
                }

                /** Update the env variable: DesQ can check this and inform the user. */
                qputenv( "__WAYFIRE_RESTART_COUNT", QByteArray::number( wfRestartCount ) );

                /**
                 * DesQ Shell is a non-gui process. It will continue to run even after a wayfire crash.
                 * Terminate it, so that we can restart the Shell GUI.
                 */
                QProcess *desqShell = new QProcess( this );
                desqShell->setProgram( "desq-shell" );
                desqShell->setArguments( { "--terminate" } );
                desqShell->start();

                /**
                 * DesQ Shell is a well-behaved app. It will terminate within a decent time.
                 * If it does not, then it means, we have a problem.
                 */
                desqShell->waitForStarted( -1 );


                /** Prevent multiple crashes in 1s: Delay the start!! */
                while ( QFile::exists( templ.arg( QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ) ) ) ) {
                    /** Sleep a little */
                    QThread::msleep( 100 );

                    /** Process the backlog of events (there won't be any :P) */
                    QCoreApplication::processEvents();
                }

                /** Reset the log file location. */
                wfLog->setFileName( templ.arg( QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ) ) );
                wfProc->start( "wayfire", wfArgs );

                /** Wait for the process to start and retrieve it's PID */
                qInfo() << "Restarting wayfire" << wfProc->waitForStarted();
                wfPID = wfProc->processId();
                qputenv( "__WF_PID", QByteArray::number( wfPID ) );
            }

            else {
                /** Start Wayfire in Safe Mode next time */
                session->setValue( "SafeMode", true );
                session->sync();

                qCritical() << "Wayfire has crashed 5 times in the past minute.";
                qCritical() << "Closing session.";

                stop();
            }
        }
    );

    /** Set log file location */
    wfLog->setFileName( templ.arg( QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ) ) );
    qputenv( "__WAYFIRE_RESTART_COUNT", QByteArray::number( wfRestartCount ) );
    wfProc->start( "wayfire", wfArgs );
    mStartDateTime = QDateTime::currentDateTime();

    /** Wait for the process to start and retrieve it's PID */
    qInfo() << "Starting wayfire" << wfProc->waitForStarted();
    wfPID = wfProc->processId();

    /** If wayfire isn't running, no point in continuing */
    if ( wfPID == 0 ) {
        qCritical() << "Wayfire is not running... Quitting the session.";
        qApp->quit();
        return;
    }

    qputenv( "__WF_PID", QByteArray::number( wfPID ) );
}


void DesQ::Session::Manager::killSessionProcesses() {
    /**
     * Session discriminator string:
     * All proc started after the session will contain this.
     * We will terminate/kill the process that contains this in the environment.
     */
    QString SESS_DISCR = qgetenv( "__DESQ_WORK_DIR" );

    /**
     * Scan all the PIDs in /proc/, read the /proc/<pid>/environ
     * If there is an entry for __DESQ_WORK_DIR which matches our work directory
     * send SIGTERM to it. At this stage all GUI processes should have been closed
     * by desq-shell. Remaining processes will be command-line processes, which
     * should know to handle SIGTERM, or get terminated.
     */
    QStringList procs;
    QDir        proc( "/proc/" );
    int         thisPID( qApp->applicationPid() );

    for ( QString pid: proc.entryList( QStringList(), QDir::Dirs | QDir::NoDotAndDotDot ) ) {
        /** Ignore Wayfire PID */
        if ( pid.toInt() == wfPID ) {
            continue;
        }

        /** Ignore this PID */
        if ( pid.toInt() == thisPID ) {
            continue;
        }

        QFile env( "/proc/" + pid + "/environ" );
        QFile cmd( "/proc/" + pid + "/cmdline" );
        cmd.open( QFile::ReadOnly );
        QFileInfo exe( "/proc/" + pid + "/exe" );

        if ( env.open( QFile::ReadOnly ) ) {
            QByteArrayList envVars = env.readAll().simplified().split( '\x00' );
            for ( QByteArray raw_envVar: envVars ) {
                QString envVar = QString::fromUtf8( raw_envVar );

                /** If this is a session process, add it to the @v procs, and send SIGTERM */
                if ( envVar.startsWith( "__DESQ_WORK_DIR" ) and envVar.endsWith( SESS_DISCR ) ) {
                    procs << "/proc/" + pid;
                    qDebug() << "Sending SIGTERM to" << pid << cmd.readAll().replace( '\x00', ' ' ) << exe.readSymLink();
                    kill( pid.toInt(), SIGTERM );
                }
            }
        }
    }

    /**
     * Wait for upto @v waitTimeOut to ensure all processes have terminated.
     * Sleep 1ms, and check if the processes are dead.
     * @v waitTimeOut and @v sleepTime are counted in ms.
     * @v waitTimeOut is always saved as seconds, so multiply by 1000.
     */
    long waitTimeOut = session->value( "WaitTimeOut", defSett->value( "WaitTimeOut" ) ).toInt() * 1000;
    long sleepTime   = 0;

    do {
        QThread::msleep( 1 );
        sleepTime += 1;

        QStringList toBeRemoved;

        /**
         * This is a rather lame check. A PID may have been reassigned.
         * However, since we're shutting down, any process started as
         * the current user will have to be killed. If the PID has been
         * reassigned to somebody else, then our kill() will not have any
         * effect.
         */
        for ( QString proc: procs ) {
            /** We may have a zombie process in our hands: it'll be reaped */
            QFile stat( proc + "/stat" );

            /** Check if this process still exists */
            if ( QFile::exists( proc ) == false ) {
                toBeRemoved << proc;
            }

            /** Check for Zombie procs */
            else if ( stat.open( QFile::ReadOnly ) && ( stat.readAll().split( ' ' ).at( 2 ) == QChar( 'Z' ) ) ) {
                toBeRemoved << proc;
                stat.close();
            }

            /** Ignore processes that do not have a valid exe */
            else if ( QFileInfo( proc + "/exe" ).readSymLink() == "" ) {
                toBeRemoved << proc;
            }

            /** Sluggish process */
            else {
                QFile cmd( proc + "/cmdline" );
                QFile stt( proc + "/stat" );

                cmd.open( QFile::ReadOnly );
                stt.open( QFile::ReadOnly );

                QFileInfo exe( proc + "/exe" );

                qDebug() << "Waiting for the sluggish process" << proc << cmd.readAll().replace( '\x00', ' ' ) << exe.readSymLink() << stt.readAll().split( ' ' ).at( 2 );
                cmd.close();
                stt.close();
            }
        }

        /** Remove the processes that are dead. */
        for ( QString proc: toBeRemoved ) {
            procs.removeAll( proc );
        }

        /** If all processes have terminated, we can quit the loop */
        if ( procs.count() == 0 ) {
            break;
        }

        qApp->processEvents();
    } while ( sleepTime < waitTimeOut );

    /**
     * Some processes may not quit after SIGTERM.
     * Some processes may not have terminated (hanged?).
     * Send SIGKILL and forget about it.
     */
    for ( QString proc: procs ) {
        int pid = proc.replace( "/proc/", "" ).toInt();
        kill( pid, SIGKILL );
    }
}


void DesQ::Session::Manager::stopWayfire() {
    /** Disconnect wfProc to prevent wayfire restart */
    wfProc->disconnect();

    /** Standard way to close wayfire */
    int ret = kill( wfPID, SIGTERM );

    if ( ret ) {
        qInfo() << "Sending SIGKILL to wayfire";
        kill( wfPID, SIGKILL );
    }
}
