# DesQSession
A Simple Session Manager for DesQ Shell


## Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* [LibDesQ](https://gitlab.com/DesQ/libdesq.git)
* [DFL::Utils](https://gitlab.com/desktop-frameworks/utils)
* [DFL::Xdg](https://gitlab.com/desktop-frameworks/xdg)
* [DFL::Applications](https://gitlab.com/desktop-frameworks/applications)
* [DFL::Login1](https://gitlab.com/desktop-frameworks/login1)

### Runtime Dependencies
* [Wayfire](https://github.com/WayfireWM/wayfire.git) - git
* [DBusQt Plugin for Wayfire](https://gitlab.com/wayfireplugins/dbusqt.git)
* [Windecor Plugin for Wayfire](https://gitlab.com/wayfireplugins/windecor.git)


## Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/Session.git DesQSession`
- Enter the `DesQSession` folder
  * `cd DesQSession`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


## Upcoming
* Any other feature you request for... :)
