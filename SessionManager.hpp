/**
 * This file is a part of DesQ Session.
 * DesQSession is the Session Manager for DesQ DE.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>
#include <QString>
#include <QDateTime>
#include <QSettings>

class SessionAdaptor;
class QProcess;

namespace DesQ {
    namespace Session {
        class Manager;
    }
}

namespace DFL {
    class Login1;
}

class DesQ::Session::Manager : public QObject {
    Q_OBJECT;

    public:
        /* Init */
        Manager();

    public Q_SLOTS:
        /* Start the session by performing various tasks */
        void start( QString session = QString() );

        /* Stop the session by saving the state */
        void stop();

        /* Initiate quit */
        bool handleMessages( const QString, int fd = 0 );

    private:
        /* Setup the global environmental variables like SESSION, etc */
        void setupEnvironmentVars();

        /* Start DesQ Shell */
        void startDesQShell();

        /* Terminate processes that were started after DesQ Session */
        void killSessionProcesses();

        /* Stop wayfire */
        void stopWayfire();

        /* Session name and type */
        QString mSessionName;

        /* Login1 */
        DFL::Login1 *login1;

        /* PID of wayfire and shell */
        QProcess *wfProc;
        qint64 wfPID;
        int wfRestartCount;
        QDateTime mStartDateTime;

        /* Wayfire config filename */
        QString wfConfig;
        QString wfConfigHj;

        /* QSettings instance */
        QSettings *session, *defSett;
};
